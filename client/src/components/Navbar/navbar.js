import React from 'react';
import { useNavigate } from 'react-router-dom';

function Navbar() {
  const navigate = useNavigate();

  const handleLogout = () => {
  
    localStorage.clear();
   
    navigate('/'); 
  };

  return (
    <nav className="bg-blue-500 p-4">
      <div className="container mx-auto">
        <div className="flex justify-between items-center">
          <div className="text-white text-xl font-semibold">My App</div>
              <button
                onClick={handleLogout}
                className="text-white hover:underline"
              >
                Logout
              </button>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
