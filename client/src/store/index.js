import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import userReducer from './reducers/user';
import todoReducer from './reducers/todo';

const rootReducer = combineReducers({
    user: userReducer,
    todo: todoReducer,
  });

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
